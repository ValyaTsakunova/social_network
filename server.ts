import {app} from './index';
import { socketStart } from './controllers/socket'

export const server = app.listen(3000, () => {
    console.log('server is listening on port 3000')
  })

const socket = require('socket.io');

export const io = socket(server);
socketStart(io);
  