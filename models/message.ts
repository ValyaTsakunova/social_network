import * as mongoose from "mongoose";
import { IChat } from "./chat";
import { IUser } from "./user";

const Schema = mongoose.Schema;

interface IMessage extends mongoose.Document {
    text: string,
    author: IUser,
    chat: IChat | string,
    date: string
}

const messageSchema = new Schema({
    text: {
        type: String, 
    }, 
    author: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "user",
        autopopulate: {
            maxDepth: 1
        }
    },
    chat: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "chat",
    },
    date: {
        type: String,
    }
},
{ versionKey: false });


messageSchema.plugin(require('mongoose-autopopulate'))
const Message = mongoose.model<IMessage>('message', messageSchema);

export { IMessage, Message };
