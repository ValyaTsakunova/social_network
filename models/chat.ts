import * as mongoose from "mongoose";
import { IUser } from "./user";

const Schema = mongoose.Schema;

interface IChat extends mongoose.Document {
    users: Array<IUser>
}

const chatSchema = new Schema({
    users: [{
        type: mongoose.Schema.Types.ObjectId,
        ref: "user",
        autopopulate: true
    }]
},
    { versionKey: false });


chatSchema.plugin(require('mongoose-autopopulate'))
const Chat = mongoose.model<IChat>('chat', chatSchema);

export { IChat, Chat };
