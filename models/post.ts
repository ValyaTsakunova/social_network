import * as mongoose from "mongoose";
import { IComment } from "./comment";
import { IUser } from "./user";

const Schema = mongoose.Schema;

interface IPost extends mongoose.Document {
    title: string;
    text: string;
    picture: string;
    video: string;
    date: string;
    likes: Array<IUser>;
    user: IUser;
    comments: Array<IComment>;
}

const postSchema = new Schema({
    title: {
        type: String,
        required: true
    },
    text: {
        type: String,
        required: true
    },
    picture: {
        type: String
    },
    video: {
        type: String
    },
    date: {
        type: String,
    },
    likes: [{
        type: mongoose.Schema.Types.ObjectId,
        ref: "user",

    }],
    user: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "user",
        autopopulate: {
            maxDepth: 1
        }
    },
    comments: [{
        type: mongoose.Schema.Types.ObjectId,
        ref: "comment",
        autopopulate: {
            maxDepth: 1
        }
    }]
},
    { versionKey: false });


postSchema.plugin(require('mongoose-autopopulate'))
const Post = mongoose.model<IPost>('post', postSchema);

export { IPost, Post };
