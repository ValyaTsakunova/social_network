import * as mongoose from "mongoose";

const Schema = mongoose.Schema;

interface IComment extends mongoose.Document {
    text: string,
    authorName: string,
    authorSurname: string,
    authorAvatar: string,
    authorId: string,
    postId: string,
    date: string
}

const commentSchema = new Schema({
    text: {
        type: String
    },
    authorName: {
        type: String
    },
    authorSurname: {
        type: String
    },
    authorAvatar: {
        type: String
    },
    authorId: {
        type: String
    },
    postId: {
        type: String,
    },
    date: {
        type: String,
    }
},
    { versionKey: false });


commentSchema.plugin(require('mongoose-autopopulate'))
const Comment = mongoose.model<IComment>('comment', commentSchema);

export { IComment, Comment };
