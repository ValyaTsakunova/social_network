import * as mongoose from "mongoose";
import { IChat } from "./chat";
import { IPost } from "./post";

const Schema = mongoose.Schema;

interface IUser extends mongoose.Document {
    name: string;
    surname: string;
    email: string;
    password: string;
    avatar: string,
    posts: Array<IPost>,
    friends: Array<IUser>,
    friendsInvitations: Array<IUser>,
    chats: Array<IChat>
}

const userSchema = new Schema({
    name: {
        type: String,
        require: true
    },
    surname: {
        type: String,
        require: true
    },
    email: {
        type: String,
        require: true,      
        unique: true       
    },
    password: {
        type: String,
        require: true
    },
    avatar: {
        type: String,
        default: 'https://thumbs.dreamstime.com/b/%D0%BA%D1%80%D0%B0%D1%81%D0%B8%D0%B2%D1%8B%D0%B9-%D0%BF%D0%B8%D0%BD%D0%B3%D0%B2%D0%B8%D0%BD-%D0%BC%D1%83%D0%BB%D1%8C%D1%82%D1%84%D0%B8%D0%BB%D1%8C%D0%BC%D0%B0-%D0%B2-%D1%82%D0%B0%D0%BD%D1%86%D0%B5%D0%B2%D0%B0%D0%BB%D1%8C%D0%BD%D1%8B%D1%85-%D0%BF%D1%80%D0%B5%D0%B7%D0%B5%D0%BD%D1%82%D0%B0%D1%86%D0%B8%D0%B8-%D0%B8%D0%B7%D0%BE%D0%BB%D1%8F%D1%86%D0%B8%D0%B8-%D0%BD%D0%B0-171521161.jpg',
        require: true
    },
    posts: [{
        type: mongoose.Schema.Types.ObjectId,
        ref: "post",
        
    }],
    friends: [{
        type: mongoose.Schema.Types.ObjectId,
        ref: "user",
        autopopulate: {
            maxDepth: 1
         }
    }],
    friendsInvitations: [{
        type: mongoose.Schema.Types.ObjectId,
        ref: "user",
        autopopulate: {
            maxDepth: 1
        }
    }],
    chats: [{
        type: mongoose.Schema.Types.ObjectId,
        ref: "chat",
        autopopulate: {
            maxDepth: 2
        }
    }]
},

    { versionKey: false });

userSchema.plugin(require('mongoose-autopopulate'))
const User = mongoose.model<IUser>('user', userSchema);

export { IUser, User };
