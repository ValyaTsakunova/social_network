import * as express from 'express';
import { getCommentsOfPost, addComment, deleteComment } from '../controllers/commentController';

const commentRouter : any = express.Router();

commentRouter.get("/getCoommentsOfPost/:id", getCommentsOfPost);
commentRouter.post("/addComment", addComment );
commentRouter.post("/deleteComment/:id", deleteComment)

export default commentRouter;