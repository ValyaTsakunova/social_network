import * as express from 'express';
import { getAllChatsByUser, findChat, sendMessage, getAllMessagesFromChat } from '../controllers/chatController'

const chatRouter : any = express.Router();

chatRouter.get("/getAllChatsByUser", getAllChatsByUser);
chatRouter.get("/findChat/:id", findChat);
chatRouter.get("/getAllMessagesFromChat/:id", getAllMessagesFromChat);

export default chatRouter;
