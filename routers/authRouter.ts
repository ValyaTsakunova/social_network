import * as express from 'express';
import { registration, login } from '../controllers/authController'

const authRouter : any = express.Router();

authRouter.post("/registration", registration);
authRouter.post("/login", login);

export default authRouter;
