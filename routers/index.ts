import authRouter from './authRouter';
import postRouter from './postRouter';
import friendsRouter from './friendsRouter';
import chatRouter from './chatRouter';
import userRouter from './userRouter';
import commentRouter from './commetRouter';

export {
    authRouter,
    postRouter,
    friendsRouter,
    chatRouter,
    userRouter,
    commentRouter
}