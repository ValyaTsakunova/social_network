import * as express from 'express';
import { changeAvatar } from '../controllers/userController'

const userRouter : any = express.Router();

userRouter.post("/changeAvatar", changeAvatar);

export default userRouter;
