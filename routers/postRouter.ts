import * as express from 'express';
import { addPost, likePost, getAllUserPosts, getNews, deletePost } from '../controllers/postController'

const postRouter : any = express.Router();

postRouter.post("/addPost", addPost);
postRouter.post("/likePost/:id", likePost);
postRouter.get("/getAllUserPosts", getAllUserPosts);
postRouter.get("/news", getNews),
postRouter.post("/deletePost/:id", deletePost)

export default postRouter;
