import * as express from 'express';
import { getAllUsers, getFriendsOfUser, addUserById, acceptInvitation, getAllInvitations, rejectInvitation, deleteuser} from '../controllers/friendsController'

const friendsRouter : any = express.Router();

friendsRouter.get("/", getFriendsOfUser)
friendsRouter.get("/allUsers", getAllUsers);
friendsRouter.post("/addFriend/:id", addUserById);
friendsRouter.post("/acceptInvitation/:id", acceptInvitation);
friendsRouter.post("/rejectInvitation/:id", rejectInvitation)
friendsRouter.get("/invitations", getAllInvitations);
friendsRouter.post("/deleteUser/:id", deleteuser)

export default friendsRouter;
