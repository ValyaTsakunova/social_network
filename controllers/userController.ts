import { User, IUser } from "../models/user";
import { Request, Response } from "express";

interface CustomRequest<T> extends Request {
    body: T;
}

export const changeAvatar = async function (request: CustomRequest<IUser>, response: Response) {
    try {
        await User.updateOne({ _id: request.user.id }, { avatar: request.body.avatar });
        return response.status(200).json(`ok`);
    } catch (e : Error | any) {
        return response.status(500).json(e.message)
    }
}