import { Request, Response } from "express";
import { IComment, Comment } from "../models/comment";
import { Post } from "../models/post";

interface CustomRequest<T> extends Request {
    body: T;
}

export const getCommentsOfPost = async function (request: CustomRequest<IComment>, response: Response) {
    try {
        const post = await Post.findOne({ _id: request.params.id });
        if (!post) {
            return response.status(500).json(`can't find post`);
        }
        return response.status(200).json(post.comments);
    } catch (e : Error | any) {
        return response.status(500).json(e.message)
    }
}

export const addComment = async function (request: CustomRequest<IComment>, response: Response) {
    try {
        const post = await Post.findOne({ _id: request.body.postId });
        if (!post) {
            return response.status(500).json(`can't find post`);
        }
        const comment = new Comment(request.body);
        await comment.save();

        post.comments.push(comment._id);
        post.save();
        return response.status(200).json(post.comments.length);
    } catch (e : Error | any) {
        return response.status(500).json(e.message)
    }
}

export const deleteComment = async function (request: CustomRequest<IComment>, response: Response) {
    try {
        await Post.updateOne({ _id: request.body.postId }, { $pull: { comments: request.params.id } });
        await Comment.deleteOne({ _id: request.params.id });
        const post = await Post.findOne({ _id: request.body.postId });
        if (!post) {
            return response.status(500).json(`can't find post`); //404
        }
        return response.status(200).json(post.comments);
    } catch (e : any) {
        return response.status(500).json(e.message)
    }
}