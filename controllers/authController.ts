import { User, IUser } from "../models/user";
import { Request, Response, NextFunction } from "express";
import * as jwt from 'jsonwebtoken';
import bcrypt from 'bcrypt';

const accessTokenSecret = 'youraccesstokensecret';

interface CustomRequest<T> extends Request {
    body: T;
}

const registration = async function (request: CustomRequest<IUser>, response: Response) {
    try {
        let user = await User.findOne({ email: request.body.email });
        if (user) {
            return response.status(400).json(`This email is already in use`)
        }
        const hashPassword = bcrypt.hashSync(request.body.password, 10);
        const newUser = new User({ ...request.body, password: hashPassword });
        await newUser.save();
        response.status(200).json(`Registration successfully.`);
    } catch (e : Error | any) {
        response.status(500).json(e.message)
    }
}

const login = async function (request: CustomRequest<IUser>, response: Response) {
    try {
        const user = await User.findOne({ email: request.body.email });
        if (!user) {
            return response.status(400).json('Wrong password or email');
        }
        if (!user || !bcrypt.compareSync(request.body.password, user.password)) {
            return response.status(400).json('Wrong password or email');
        }
        const accessToken = jwt.sign({ name: user.name, id: user._id }, accessTokenSecret, { expiresIn: '56565656m' });
        response.status(200).json({
            user: user,
            id: user._id,
            accessToken: accessToken,
        });
    } catch (e : Error | any) {
        response.status(401).json(e.message)
    }
}

const authenticateJWT = (request: CustomRequest<IUser>, response: Response, next: NextFunction) => {
    const authHeader = request.headers.authorization;
    if (authHeader) {
        const token = authHeader.split(' ')[1];
        jwt.verify(token, accessTokenSecret, (err, user) => {
            if (err) {
                return response.sendStatus(403);
            }
            request.user = user as IUser;
            next();
        });
    } else {
        response.sendStatus(401);
    }
}

export {
    registration,
    login,
    authenticateJWT
}
