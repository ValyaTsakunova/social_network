import { Post, IPost } from "../models/post";
import { User } from "../models/user";
import { Comment } from '../models/comment';
import { Request, Response } from "express";

interface CustomRequest<T> extends Request {
    body: T;
}

const addPost = async function (request: CustomRequest<IPost>, response: Response) {
    try {
        const user = await User.findOne({ _id: request.user.id });
        if (!user) {
            return response.status(500).json(`Can't find user`);
        }
        const post = new Post(request.body);
        post.user = request.user.id;
        await post.save();

        user.posts.push(post._id);
        await user.save();

        const posts = await Post.find({ user: request.user.id });
        if (!posts) {
            return response.status(500).json(`can't find post`);
        }
        return response.status(200).json(posts);
    } catch (e : Error | any) {
        return response.status(500).json(e.message)
    }
}

const likePost = async function (request: CustomRequest<IPost>, response: Response) {
    try {
        const post = await Post.findOne({ _id: request.params.id });
        if (!post) {
            return response.status(500).json(`can't find post`);
        }
        const user = await User.findOne({ _id: request.user.id });
        if (!user) {
            return response.status(500).json(`can't find user`);
        }
        const isAlreadyLiked = post.likes.indexOf(user._id);
        if (isAlreadyLiked == -1) {
            post.likes.push(user._id);
            await post.save();
        } else {
            await Post.updateOne({ _id: post._id }, { $pull: { likes: user._id } });
        }
        return response.status(200).json('ok');
    } catch (e : Error | any) {
        return response.status(500).json(e.message)
    }
}

const getAllUserPosts = async function (request: CustomRequest<IPost>, response: Response) { //Iuser
    try {
        const user = await User.findOne({ _id: request.user.id });
        if (!user) {
            return response.status(500).json(`can't find user`);
        }
        const posts = await Post.find({ user: request.user.id });
        if (!posts) {
            return response.status(500).json(`can't find post`);
        }
        response.status(200).json(posts)
    } catch (e : Error | any) {
        response.status(401).json(e.message)
    }
}

const getNews = async function (request: CustomRequest<IPost>, response: Response) { //Iuser
    try {
        const user: any = await User.findOne({ _id: request.user.id }); //
        if (!user) {
            return response.status(500).json(`can't find user`);
        }
        let friendPosts = await Post.find({ user: user.friends })
        return response.status(200).json(friendPosts)
    } catch (e : Error | any) {
        return response.status(401).json(e.message)
    }
}

const deletePost = async function (request: CustomRequest<IPost>, response: Response) {
    try {
        await User.updateOne({ _id: request.user.id }, { $pull: { posts: request.params.id } });
        await Post.deleteOne({ _id: request.params.id });
        await Comment.deleteMany({ postId: request.params.id })
        const posts = await Post.find({ user: request.user.id });
        if (!posts) {
            return response.status(500).json(`can't find post`);
        }
        return response.status(200).json(posts)
    } catch (e : Error | any) {
        return response.status(401).json(e.message)
    }
}

export {
    addPost,
    likePost,
    getAllUserPosts,
    getNews,
    deletePost
}
