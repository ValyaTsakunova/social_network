import { Request, Response } from "express";
import { Chat } from "../models/chat";
import { User, IUser } from "../models/user";
import { Message } from '../models/message';

interface CustomRequest<T> extends Request {
    body: T;
}

const getFriendsOfUser = async function (request: CustomRequest<IUser>, response: Response) {
    try {
        const user = await User.findOne({ _id: request.user.id });
        if (!user) {
            return response.status(500).json(`can't find user`);
        }
        return response.status(200).json(user.friends)
    } catch (e : Error | any) {
        return response.status(500).json(`${e.message}`)
    }
}

const getAllUsers = async function (request: CustomRequest<IUser>, response: Response) {
    try {
        const users = await User.find();
        if (!users) {
            return response.status(500).json(`can't find users`);
        }
        return response.status(200).json(users)
    } catch (e : Error | any) {
        return response.status(500).json(`${e.message}`)
    }
}

const addUserById = async function (request: CustomRequest<IUser>, response: Response) {
    try {
        const user = await User.findOne({ _id: request.user.id });
        if (!user) {
            return response.status(500).json(`can't find user`);
        }
        const friend = await User.findOne({ _id: request.params.id });
        if (!friend) {
            return response.status(500).json(`can't find friend`);
        }
        const alreadyFriends = friend.friendsInvitations.find(item => item._id.toString() == user._id.toString());
        if (!alreadyFriends) {
            friend.friendsInvitations.push(user._id);
            friend.save();
            return response.status(200).json(`added friend`)
        }
        return response.status(200).json(`you are already friends`)
    } catch (e : Error | any) {
        return response.status(500).json(`${e.message}`)
    }
}

const getAllInvitations = async function (request: CustomRequest<IUser>, response: Response) {
    try {
        const user = await User.findOne({ _id: request.user.id });
        if (!user) {
            return response.status(500).json(`can't find user`);
        }
        if (user.friendsInvitations) {
            return response.status(200).json(user.friendsInvitations)
        }
        return response.status(200).json(`you don't have invitations`)
    } catch (e : Error | any) {
        return response.status(500).json(`${e.message}`)
    }
}

const acceptInvitation = async function (request: CustomRequest<IUser>, response: Response) {
    try {
        const user = await User.findOne({ _id: request.user.id });
        if (!user) {
            return response.status(500).json(`can't find user`);
        }
        const friend = await User.findOne({ _id: request.params.id });
        if (!friend) {
            return response.status(500).json(`can't find friend`);
        }
        const alreadyFriends = user.friends.find(item => item._id == friend._id);
        if (alreadyFriends) {
            return response.status(200).json(`already friends`)
        }
        const chat = new Chat({ users: [friend._id, user._id] });
        await chat.save();

        user.friends.push(friend._id);
        user.chats.push(chat._id);
        await User.updateOne({ _id: user._id }, { $pull: { friendsInvitations: friend._id } });
        await user.save();

        friend.chats.push(chat._id);
        friend.friends.push(user._id);
        await User.updateOne({ _id: friend._id }, { $pull: { friendsInvitations: user._id } });
        await friend.save();

        const userUpdated = await User.findOne({ _id: request.user.id });
        if (!userUpdated) {
            return response.status(500).json(`can't find user`);
        }
        return response.status(200).json(userUpdated.friendsInvitations)
    } catch (e : Error | any) {
        return response.status(500).json(`${e.message}`)
    }
}

const rejectInvitation = async function (request: CustomRequest<IUser>, response: Response) {
    try {
        const user = await User.findOne({ _id: request.user.id });
        if (!user) {
            return response.status(500).json(`can't find user`);
        }
        const friend = await User.findOne({ _id: request.params.id });
        if (!friend) {
            return response.status(500).json(`can't find friend`);
        }
        await User.updateOne({ _id: request.user.id }, { $pull: { friendsInvitations: friend._id } });
        const userUpdated = await User.findOne({ _id: request.user.id });
        if (!userUpdated) {
            return response.status(500).json(`can't find user`);
        }
        return response.status(200).json(userUpdated.friendsInvitations)
    } catch (e : Error | any) {
        return response.status(500).json(`${e.message}`)
    }
}

const deleteuser = async function (request: CustomRequest<IUser>, response: Response) {
    try {
        const user = await User.findOne({ _id: request.user.id });
        if (!user) {
            return response.status(500).json(`can't find user`);
        }
        const friend = await User.findOne({ _id: request.params.id });
        if (!friend) {
            return response.status(500).json(`can't find friend`);
        }
        const chat = await Chat.findOne({ $or: [{ users: [friend._id, user._id] }, { users: [user._id, friend._id] }] });
        if (!chat) {
            return response.status(500).json(`can't find chat`);
        }
        await User.updateOne({ _id: request.user.id }, { $pull: { friends: friend._id } }, { new: true });
        await User.updateOne({ _id: request.user.id }, { $pull: { chats: chat._id } }, { new: true });

        await User.updateOne({ _id: request.params.id }, { $pull: { friends: user._id } }, { new: true });
        await User.updateOne({ _id: request.params.id }, { $pull: { chats: chat._id } }, { new: true });

        await Message.deleteMany({ chat: chat._id });

        await chat.deleteOne();

        const userUpdated = await User.findOne({ _id: request.user.id });
        if (!userUpdated) {
            return response.status(500).json(`can't find user`);
        }
        return response.status(200).json(userUpdated.friends)
    } catch (e : Error | any) {
        return response.status(500).json(`${e.message}`)
    }
}

export {
    getAllUsers,
    getFriendsOfUser,
    addUserById,
    acceptInvitation,
    getAllInvitations,
    rejectInvitation,
    deleteuser
}