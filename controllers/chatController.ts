import { User } from "../models/user";
import { Request, Response } from "express";
import { Chat, IChat } from "../models/chat";
import { IMessage, Message } from "../models/message";

interface CustomRequest<T> extends Request {
    body: T;
}

const findChat = async function (request: CustomRequest<IChat>, response: Response) {
    try {
        const user = await User.findOne({ _id: request.user.id });
        if (!user) {
            return response.status(500).json(`Can't find user`);
        }
        const friend = await User.findOne({ _id: request.params.id });
        if (!friend) {
            return response.status(500).json(`can't find friend`);
        }
        const chat = await Chat.findOne({ $or: [{ users: [friend._id, user._id] }, { users: [user._id, friend._id] }] });
        if (!chat) {
            return response.status(500).json(`can't find chat`);
        }
        return response.status(200).json(chat._id);
    } catch (e :any) {
        return response.status(500).json(e.message)
    }
}

const getAllChatsByUser = async function (request: CustomRequest<IChat>, response: Response) {
    try {
        const user = await User.findOne({ _id: request.user.id });
        if (!user) {
            return response.status(500).json(`Can't find user`);
        }
        return response.status(200).json(user.chats);
    } catch (e : any) {
        return response.status(500).json(e.message)
    }
}

const sendMessage = async function (chatId : any, message : any) {
    try {
        const mess = new Message(message);
        await mess.save();
        const arrOfMess = await Message.find({ chat: chatId });
        return arrOfMess
    } catch (e : any) {
        return 
    }
}

const getAllMessagesFromChat = async function (request: CustomRequest<IMessage>, response: Response) {
    try {
        const messages = await Message.find({ chat: request.params.id });
        if (!messages) {
            return response.status(500).json(`can't find chat`); ///
        }
        return response.json(messages); 
    } catch (e : any) {
        return response.status(500).json(e.message)
    }
}

export {
    findChat,
    getAllChatsByUser,
    sendMessage,
    getAllMessagesFromChat
}