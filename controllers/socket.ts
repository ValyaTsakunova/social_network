import { Socket } from 'socket.io'
import { sendMessage } from './chatController';

export const socketStart = async (io: any) => {
  try {
    io.on("connection", (socket: Socket) => {
      console.log(`user connect`)
      socket.on('send message', async ({chatId, message} : any) => {
        const arrOfMess = await sendMessage(chatId, message);
        io.emit("get messages for this chat", arrOfMess);
      })
    })
  } catch (e : Error | any) {
    console.log(e)
  }
}