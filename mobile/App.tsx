import React, { useReducer } from 'react';
import { StyleSheet, Text, View } from 'react-native';
import { MainNav } from './src/navigation/MainNav';
import { AppState } from './src/context/state'

export default function App() {

  return (
    <>
    <AppState>
       <MainNav />
    </AppState>
    </>
  )
}

