import * as yup from 'yup';

export const registrationSchema = yup.object().shape({
  name: yup.string().min(2).required(),
  surname: yup.string().min(2).required(),
  email: yup.string().email('enter correct email').required(),
  password: yup.string().min(4).required()
});