import * as yup from 'yup';

export const loginSchema = yup.object().shape({
  email: yup.string().email('invalid email').required(),
  password: yup.string().min(4).required()
});