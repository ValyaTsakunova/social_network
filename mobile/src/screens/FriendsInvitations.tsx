import React, { useEffect, useState } from 'react';
import { View, StyleSheet, Text, ScrollView, ImageBackground } from 'react-native';
import { IUser } from '../context/context';
import { InvitationItem } from '../components/InvitationItem';
import { getAllInvitations } from '../services/http.service';

export function FriendsInvitations({route} : any) {
    const [invit, setInvit] = useState([])

    useEffect(() => {
        (async () => {
            const res = await getAllInvitations();
            setInvit(res);
        })()
    }, [route.params])

    return (
        <ImageBackground style={styles.img} source={require('../../assets/backgrounds/back5.png')}>
            <ScrollView >
                <View style={styles.block}>
                    {invit.length !== 0 ? invit?.map((user: IUser) => {
                        return (
                            <InvitationItem user={user} key={user._id} setInvit={setInvit} />
                        )}) : <Text style={styles.text}>You don't have invitations</Text>}
                </View>
            </ScrollView>
        </ImageBackground>
    )
}

const styles = StyleSheet.create({
    img: {
        height: '100%',
        width: "100%",
        position: 'absolute',
        top: 0,
        left: 0,
    },
    text: {
        color: 'white',
        fontFamily: 'Noto Sans KR',
        fontSize: 25,
    },
    block: {
        flex: 1,
        alignItems: 'center'
    }
})
