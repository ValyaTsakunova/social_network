import React, { useState, useEffect } from 'react';
import { View, StyleSheet, Text, TouchableOpacity, ImageBackground, ScrollView } from 'react-native';
import { IPost } from '../context/context';
import { PostItem } from '../components/PostItem';
import { ModalNewPost } from '../components/ModalNewPost';
import { getAllUserPosts } from '../services/http.service';

interface props {
  route: any
}

export function Posts({ route }: props) {
  const [modalVisible, setModalVisible] = useState(false);
  const [userPosts, setUserPosts] = useState<Array<IPost>>([]);

  useEffect(() => {
    (async () => {
      const res = await getAllUserPosts();
      setUserPosts(res);
    })()
  }, [route.params])

  return (
    <ImageBackground style={styles.img} source={require('../../assets/backgrounds/back5.png')}>
      <View style={styles.block}>
        {modalVisible ? <ModalNewPost setModalVisible={setModalVisible} setUserPosts={setUserPosts} /> : null}
        <ScrollView keyboardShouldPersistTaps='always'>
          {userPosts?.length !== 0 ? userPosts?.map(post => <PostItem key={post._id} post={post} setUserPosts={setUserPosts} />) : null}
        </ScrollView>
        <TouchableOpacity onPress={() => setModalVisible(true)}>
          <Text style={styles.addPost}>Add post</Text>
        </TouchableOpacity>
      </View>
    </ImageBackground>

  )
}

const styles = StyleSheet.create({
  block: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    paddingTop: 10
  },
  scroll: {
    height: '100%'
  },
  text: {
    fontSize: 27,
    color: 'white',
    fontFamily: 'Noto Sans KR'
  },
  addPost: {
    margin: 10,
    color: "white",
    fontSize: 16,
    paddingLeft: 10,
    paddingRight: 10,
    borderRadius: 10,
    backgroundColor: "#9932CC",
    fontFamily: 'Noto Sans KR'
  },
  img: {
    height: '100%',
    width: "100%",
    position: 'absolute',
    top: 0,
    left: 0,
  }
})
