import React, { useState, useContext } from 'react';
import { View, TextInput, StyleSheet, Button, Text, TouchableHighlight, ImageBackground, Keyboard } from 'react-native';
import { useEffect } from 'react';
import { Formik } from 'formik';
import { login, getAllInvitations } from '../services/http.service';
import Context from '../context/context';
import { Loader } from '../components/Loader';
import { loginSchema } from '../validation/login';

export function Login({ navigation, route }: any) {
  const context = useContext(Context);
  const { setUserInfo, getInvitations } = context;

  const [loading, setloading] = useState(false);

  useEffect(() => {
    setloading(false)
  }, [route.params])

  const handleLogIn = async ({ email, password }: { email: string, password: string }) => {
    Keyboard.dismiss();
    setloading(true)
    const result = await login({ email, password });
    if (typeof result !== 'boolean' && setUserInfo) {
      await setUserInfo(result);
      getInvitations && getInvitations(await getAllInvitations());
      navigation.navigate('profile');
    } else {
      setloading(false)
    }
  }

  return (
    <ImageBackground style={styles.img} source={require('../../assets/backgrounds/back2.jpg')}>
      {loading ?
        <Loader /> :
        <View style={styles.form}>
          <Formik
            validationSchema={loginSchema}
            initialValues={{ email: '', password: '' }}
            onSubmit={values => handleLogIn(values)}
          >
            {({
              handleBlur,
              handleChange,
              handleSubmit,
              values,
              errors,
              isValid
            }) => (
              <>
                <TextInput
                  placeholder="type your email"
                  style={styles.input}
                  onChangeText={handleChange('email')}
                  onBlur={handleBlur('email')}
                  value={values.email}
                />
                {errors.email &&
                  <Text style={styles.errors}>{errors.email}</Text>
                }
                <TextInput
                  placeholder="type your password"
                  style={styles.input}
                  onChangeText={handleChange('password')}
                  onBlur={handleBlur('password')}
                  value={values.password}
                  secureTextEntry
                />
                {errors.password &&
                  <Text style={styles.errors}>{errors.password}</Text>
                }
                <Button title="Log in" onPress={() => handleSubmit()} color="#612751" disabled={!isValid} />
                <TouchableHighlight underlayColor="transparent" onPress={() => navigation.navigate("Registration")} >
                  <Text style={styles.registration}>Registration</Text>
                </TouchableHighlight>
              </>
            )}
          </Formik>
        </View>}
    </ImageBackground>
  )
}

const styles = StyleSheet.create({
  indicator: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  form: {
    flex: 2,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
  },
  input: {
    width: '80%',
    borderColor: 'black',
    color: 'white',
    borderWidth: 1,
    borderStyle: 'solid',
    borderRadius: 10,
    paddingLeft: 15,
    padding: 5,
    marginBottom: 10,
    fontSize: 16,
    backgroundColor: 'rgba(247, 239, 246, 0.6)',
  },
  registration: {
    marginTop: 15,
    fontSize: 18,
    color: 'white',
    textDecorationLine: 'underline'
  },
  img: {
    height: '100%',
    width: "100%",
    position: 'absolute',
    top: 0,
    left: 0
  },
  errors: {
    fontSize: 12,
    color: 'white',
    marginBottom: 10,
    marginTop: -10,
    borderBottomColor: 'red',
    borderBottomWidth: 1
  }
})
