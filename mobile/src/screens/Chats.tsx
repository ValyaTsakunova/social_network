import React, { useState, useEffect } from 'react';
import { View, StyleSheet, Text, ScrollView, ImageBackground } from 'react-native';
import { getUserChats } from '../services/http.service';
import { ChatItem } from '../components/ChatItem';

export function Chats({ route }: any) {
    const [chats, setChats] = useState([]);

    useEffect(() => {
        (async () => {
            const res = await getUserChats();
            setChats(res);
        })()
    }, [route.params]);

    return (
        <ImageBackground style={styles.img} source={require('../../assets/backgrounds/back4.png')}>
            <View style={styles.block}>
                <Text style={styles.text}>Chats</Text>
                <ScrollView >
                    {chats.length !== 0
                        ? chats.map((chat: any) => {
                            return (<ChatItem chat={chat} key={chat._id} />)
                        })
                        : null
                    }
                </ScrollView>
            </View>
        </ImageBackground>
    )
}

const styles = StyleSheet.create({
    block: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },
    img: {
        height: '100%',
        width: "100%",
        position: 'absolute',
        top: 0,
        left: 0
    },
    text: {
        color: 'white',
        fontFamily: 'Noto Sans KR',
        fontSize: 25,
    },
})