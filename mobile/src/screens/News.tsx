import React, {useState, useEffect} from 'react';
import { View, StyleSheet, ScrollView, ImageBackground } from 'react-native';
import { IPost } from '../context/context';
import { getFriendsNews } from '../services/http.service';
import { PostItem } from '../components/PostItem';

export function News({route} : any){
    const [friendsNews, setFriendsNews] = useState([])
    
    useEffect(() => {
        (async () => {
            const res = await getFriendsNews();
            setFriendsNews(res);
        })()
    }, [route.params])
   
    return(
        <ImageBackground style={styles.img} source={require('../../assets/backgrounds/back.png')}>
           <View  style={styles.block}>
            <ScrollView keyboardShouldPersistTaps='always'>
             {friendsNews?.length !== 0 ? friendsNews?.map((post : IPost) => {return(<PostItem key={post._id} post={post} setFriendsNews={setFriendsNews} />)}) : null }
            </ScrollView>
        </View>
        </ImageBackground>
    )
}

const styles = StyleSheet.create({
    block: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        paddingTop: 20,
      },
    img: {
        height: '100%',
        width: "100%",
        position: 'absolute',
        top: 0,
        left: 0
    }
})
