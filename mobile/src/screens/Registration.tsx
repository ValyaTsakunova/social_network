import React from 'react';
import { View, TextInput, Button, StyleSheet, Text, ImageBackground } from 'react-native';
import { registration } from '../services/http.service'
import { registrationSchema } from '../validation/registration';
import { Formik } from 'formik';

export function Registration({ navigation }: any) {

    const saveInfo = async (values: any) => {
        const result = await registration(values);
        if (result == true) {
            navigation.navigate('Login');
        }
    }

    return (
        <ImageBackground style={styles.img} source={require('../../assets/backgrounds/back3.png')}>
            <View style={styles.form}>
                <Formik
                    validationSchema={registrationSchema}
                    initialValues={{ name: '', surname: '', email: '', password: '' }}
                    onSubmit={values => saveInfo(values)}
                >
                    {({
                        handleBlur,
                        handleChange,
                        handleSubmit,
                        values,
                        errors,
                        isValid
                    }) => (
                        <>
                            <TextInput
                                placeholder="type your name"
                                style={styles.input}
                                onChangeText={handleChange('name')}
                                onBlur={handleBlur('name')}
                                value={values.name}
                            />
                            {errors.name &&
                                <Text style={styles.errors}>{errors.name}</Text>
                            }
                            <TextInput
                                placeholder="type your surname"
                                style={styles.input}
                                onChangeText={handleChange('surname')}
                                onBlur={handleBlur('surname')}
                                value={values.surname}
                            />
                            {errors.surname &&
                                <Text style={styles.errors}>{errors.surname}</Text>
                            }
                            <TextInput
                                placeholder="type your email"
                                style={styles.input}
                                onChangeText={handleChange('email')}
                                onBlur={handleBlur('email')}
                                value={values.email}
                            />
                            {errors.email &&
                                <Text style={styles.errors}>{errors.email}</Text>
                            }
                            <TextInput
                                placeholder="type your password"
                                style={styles.input}
                                onChangeText={handleChange('password')}
                                onBlur={handleBlur('password')}
                                value={values.password}
                                secureTextEntry
                            />
                            {errors.password &&
                                <Text style={styles.errors}>{errors.password}</Text>
                            }
                            <Button title='Sign up' onPress={() => handleSubmit()} color="#800080" disabled={!isValid} />
                        </>
                    )}
                </Formik>
            </View>
        </ImageBackground >
    )
}

const styles = StyleSheet.create({
    form: {
        flex: 1,
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
    },
    input: {
        width: '80%',
        borderColor: 'black',
        borderWidth: 2,
        borderStyle: 'solid',
        marginBottom: 10,
        padding: 5,
        fontSize: 16,
        borderRadius: 10,
        paddingLeft: 15,
        backgroundColor: 'rgba(247, 239, 246, 0.8)',
    },
    img: {
        height: '100%',
        width: "100%",
        position: 'absolute',
        top: 0,
        left: 0
    },
    errors: {
        fontSize: 12,
        color: 'white',
        marginBottom: 10,
        marginTop: -10,
        borderBottomColor: 'red',
        borderBottomWidth: 1
    }
})
