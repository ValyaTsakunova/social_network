import React, { useContext, useState, useEffect,  useRef } from 'react';
import { View, StyleSheet, TouchableOpacity, Text, Alert, Image, ScrollView, TextInput, Keyboard, ImageBackground } from 'react-native';
import { MaterialCommunityIcons } from '@expo/vector-icons';
import Context, { IMessage } from '../context/context';
import { getAllMessagesFromChat, socket } from '../services/http.service';
import { MessageItem } from '../components/MessageItem';
import dateFormat from 'dateformat';

export function CurrentChat(props: any) {
  const context = useContext(Context);
  const { userInfo } = context;

  const [messageText, setMessageText] = useState('');
  const [messages, setMessages] = useState<Array<IMessage>>([]);

  const { user, chatId } = props.route.params;
  const scrollViewRef = useRef<any>()

  useEffect((): any => {
    socket.connect();
    (async () => {
      const res = await getAllMessagesFromChat(chatId);
      setMessages(res);
    })()
    return () => socket.disconnect()
  }, []);

  useEffect(() => {
    socket.on('get messages for this chat', (mess: any) => {
      setMessages(mess)
    })
  }, [])

  useEffect(() => {
    scrollViewRef.current?.scrollToEnd({ animated: true })
  }, [messages.length])

  const sendMess = async () => {
    if (!messageText) {
      return Alert.alert(`Warning`, `Message should't be empty`)
    }
    Keyboard.dismiss();
    const message = {
      author: userInfo.id,
      text: messageText,
      chat: chatId,
      date: dateFormat(new Date(), "isoTime")
    }
    setMessageText('');
    socket.emit('send message', {chatId, message});
  }

  return (
    <ImageBackground style={styles.back} source={require('../../assets/backgrounds/back.png')}>
    <View style={styles.block}>
      <View style={styles.info}>
        <Image style={styles.img} source={{ uri: user.avatar }} />
        <Text style={styles.text}>{user.name} {user.surname}</Text>
      </View>
      <ScrollView style={styles.messageBlock} ref={scrollViewRef}>
        {messages.map((message: IMessage) => {
          return (
            <MessageItem message={message} key={message._id} />)
        })}
      </ScrollView>
      <View style={styles.sendMess}>
        <TextInput
          style={styles.input}
          multiline={true}
          placeholder='Message here...'
          value={messageText}
          onChangeText={setMessageText}
        />
        <TouchableOpacity onPress={sendMess}>
          <MaterialCommunityIcons name="send" size={25} color="white" />
        </TouchableOpacity>
      </View>

    </View>
    </ImageBackground>
  )
}

const styles = StyleSheet.create({
  block: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
    width: '100%'
  },
  messageBlock: {
    width: '95%',
  },
  input: {
    color: 'white',
    padding: 5,
    width: '90%',
    borderBottomWidth: 1,
    borderColor: 'white',
    fontSize: 20
  },
  sendMess: {
    flexDirection: 'row',
    margin: 10,
    alignItems: 'center',
  },
  img: {
    width: 50,
    height: 50,
    borderRadius: 50,
    marginRight: 10,
  },
  info: {
    alignItems: 'center',
    paddingLeft: 20,
    flexDirection: 'row',
    width: '90%',
    borderBottomWidth: 1,
    borderColor: 'black'
  },
  text: {
    fontSize: 20,
    color: 'black',
    fontFamily: 'Noto Sans KR',
  },
  loader: {
    marginTop: 50
  },
  back: {
    height: '100%',
    width: "100%",
    position: 'absolute',
    top: 0,
    left: 0,
  },
})