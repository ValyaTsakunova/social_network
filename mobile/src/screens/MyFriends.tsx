import React, { useEffect, useState } from 'react';
import { View, StyleSheet, Text, ScrollView, ImageBackground } from 'react-native';
import { IUser } from '../context/context';
import { FriendItem } from '../components/FriendItem';
import { getUserFriends } from '../services/http.service';

export function MyFriends({route} : any) {
    const [friends, setFriends] = useState([])

    useEffect(() => {
        (async () => {
            const res = await getUserFriends();
            setFriends(res);
        })()
    }, [route.params])

    return (
        <ImageBackground style={styles.img} source={require('../../assets/backgrounds/back4.png')}>
            <View style={styles.block}>
                <ScrollView>
                    {friends.length !== 0 ? friends?.map((user: IUser) => { return (<FriendItem key={user._id} user={user} setFriends={setFriends} />) }) : null}
                </ScrollView>
            </View>
        </ImageBackground>
    )
}

const styles = StyleSheet.create({
    img: {
        height: '100%',
        width: "100%",
        position: 'absolute',
        top: 0,
        left: 0,
    },
    text: {
        color: 'white',
        fontFamily: 'Noto Sans KR',
        fontSize: 25,
    },
    block: {
        flex: 1,
        alignItems: 'center'
    }
})
