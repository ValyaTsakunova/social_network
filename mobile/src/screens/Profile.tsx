import React, { useContext, useState } from 'react';
import { View, StyleSheet, Text, Image, TouchableOpacity, ImageBackground, Platform } from 'react-native';
import Context from '../context/context';
import { useFonts } from 'expo-font';
import AppLoading from "expo-app-loading";
import * as ImagePicker from 'expo-image-picker';
import * as MediaLibrary from 'expo-media-library';
import { changeUserAvatar } from '../services/http.service';

export function Profile() {
    const context = useContext(Context);
    const { userInfo } = context;
    const [avat, setAvat] = useState(userInfo.avatar);

    let [fontsLoaded] = useFonts({
        'Noto Sans KR': require('../../assets/fonts/NotoSansKR-Regular.otf'),
    });
    if (!fontsLoaded) {
        return <AppLoading />;
    }

    const changeAvatar = async () => {
        (async () => {
            if (Platform.OS !== 'web') {
                const { status } = await ImagePicker.requestMediaLibraryPermissionsAsync();
                if (status !== 'granted') {
                    return false
                }
                return true
            }
        })();
        let result = await ImagePicker.launchImageLibraryAsync({
            mediaTypes: ImagePicker.MediaTypeOptions.All,
            allowsEditing: true,
            aspect: [4, 3],
            quality: 1,
        });

        if (!result.cancelled) {
            const asset = await MediaLibrary.createAssetAsync(result.uri);
            setAvat(asset.uri);
            await changeUserAvatar(asset.uri)
        }
    }

    return (
        <ImageBackground style={styles.img} source={require('../../assets/backgrounds/back4.png')}>
            <View style={styles.block}>
                <Text style={styles.welcome}>Welcome to your home page!</Text>
                <Image source={{ uri: avat }} style={styles.avatar} />
                <TouchableOpacity onPress={changeAvatar}>
                    <Text style={styles.change}>Change avatar</Text>
                </TouchableOpacity>
                <View style={styles.info}>
                    <Text style={styles.name}>{userInfo.name} </Text>
                    <Text style={styles.surname}>{userInfo.surname}</Text>
                </View>
            </View>
        </ImageBackground>
    )
}

const styles = StyleSheet.create({
    welcome: {
        fontSize: 25,
        color: 'white',
        fontFamily: 'Noto Sans KR',
    },
    block: {
        flex: 1,
        alignItems: 'center',
    },
    info: {
        flex: 1,
        width: 500,
        alignItems: 'center',
        justifyContent: 'flex-start',
        backgroundColor: 'rgba(247, 239, 246, 0.6)',
        marginTop: 70,
        borderTopLeftRadius: 300,
        borderTopRightRadius: 300,
    },
    name: {
        fontSize: 30,
        fontFamily: 'Noto Sans KR',
    },
    surname: {
        fontSize: 25,
        marginTop: -30,
        fontFamily: 'Noto Sans KR',
    },
    avatar: {
        borderWidth: 1,
        borderColor: 'white',
        borderRadius: 200,
        width: 150,
        height: 150,
        marginTop: 30
    },
    change: {
        color: 'white',
        textDecorationLine: 'underline'
    },
    img: {
        height: '100%',
        width: "100%",
        position: 'absolute',
        top: 0,
        left: 0
    }
})
