import React, { useState, useEffect } from 'react';
import { View, StyleSheet, TextInput, ScrollView, ImageBackground } from 'react-native';
import { IUser } from '../context/context';
import { UserItem } from '../components/UserItem';
import { getUsers } from '../services/http.service';

export function FindFriends({ route }: any) {
  const [inputValue, setInputValue] = useState('');
  const [users, setUsers] = useState<Array<IUser>>([]);

  useEffect(() => {
    (async () => {
      const res = await getUsers();
      setUsers(res);
    })()
  }, [route.params])

  return (
    <ImageBackground style={styles.img} source={require('../../assets/backgrounds/back.png')}>
      <View style={styles.block}>
        <TextInput style={styles.input} value={inputValue} placeholder="type here ..." onChangeText={text => setInputValue(text)} />
        <ScrollView>
          <View>
            {users.length !== 0 ? users?.
              filter((user: any) => (user.name.toLowerCase() + user.surname.toLowerCase()).
                includes(inputValue.toLowerCase())).
              map(user => { return (<UserItem user={user} key={user._id} />) }) : null}
          </View>
        </ScrollView>
      </View>
    </ImageBackground>
  )
}

const styles = StyleSheet.create({
  input: {
    width: '85%',
    color: 'black',
    borderRadius: 10,
    margin: 10,
    paddingLeft: 15,
    padding: 5,
    fontSize: 16,
    backgroundColor: 'rgba(247, 239, 246, 0.8)',
  },
  findButton: {
    backgroundColor: 'pink',
    width: 50,
  },
  img: {
    height: '100%',
    width: "100%",
    position: 'absolute',
    top: 0,
    left: 0,
  },
  block: {
    flex: 1,
    alignItems: 'center'
  }
})
