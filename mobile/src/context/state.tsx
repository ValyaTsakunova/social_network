import React, { useReducer } from 'react';
import { reducer } from './reducer';
import { SET_USER_INFO, GET_INVITATIONS } from './constants';
import Context from './context';
import { IUserData, IUser } from './context';

const defaultState = {
  userInfo: {
    name: '',
    surname: '',
    email: '',
    password: '',
    id: ''
  },
  invitations: [],
};


export function AppState({ children }: any) {

  const [state, dispatch] = useReducer(reducer, defaultState);

  const setUserInfo = (data: IUserData) => dispatch({ type: SET_USER_INFO, data });
  const getInvitations = (invitations: Array<IUser>) => dispatch({ type: GET_INVITATIONS, invitations });

  return <Context.Provider value={{
    userInfo: state.userInfo,
    invitations: state.invitations,
    setUserInfo,
    getInvitations,
  }}>
    {children}
  </Context.Provider>
}