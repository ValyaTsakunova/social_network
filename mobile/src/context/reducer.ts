import { SET_USER_INFO, GET_INVITATIONS } from "./constants";

export const reducer = (state: any, action: any) => {
  switch (action.type) {
    case SET_USER_INFO:
      return { ...state, userInfo: action.data }

    case GET_INVITATIONS:
      return { ...state, invitations: action.invitations }

    default:
      return state;
  }
}


