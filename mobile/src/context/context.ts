import React from "react";

export interface IUserData {
  name: string,
  surname: string,
  email: string,
  password: string,
  id: string, 
  avatar: string
}

export interface IPost {
  _id?: string,
  title: string,
  text: string,
  date: string,
  likes?: Array<string>,
  picture?: string,
  video?: string,
  user?: IUser,
  comments?: Array<IComment>
}

export interface IUser {
  _id: string,
  name: string,
  surname: string,
  avatar: string,
  friends: Array<IUser>
}

export interface IChat {
  _id: string,
  users: Array<IUser>
}

export interface IMessage {
  text: string,
  author: IUser,
  chat: IChat,
  _id?: string,
  date: string
}

export interface IComment {
  text: string,
  authorName: string,
  authorSurname: string,
  authorAvatar: string,
  authorId: string,
  postId: string | any,
  _id?: string,
  date: string
}

type IContext = {
  userInfo?: any,
  invitations?: Array<IUser>,
  setUserInfo?: (data: IUserData) => void,
  getInvitations?: (invitations: Array<IUser>) => void,
};

const Context = React.createContext<IContext>({});

export default Context;




