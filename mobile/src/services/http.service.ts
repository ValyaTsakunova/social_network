import { Alert } from 'react-native';
import { io } from "socket.io-client";
import { getTokenInfo, storeTokenInfo } from './asyncStorage';
import { IUserData, IPost, IComment } from '../context/context';

const URL = 'http://192.168.1.130:3000';

export const socket = io(URL, {autoConnect: false} );

interface IData {
    name?: string,
    surname?: string,
    email: string,
    password: string
};

export async function registration(data: IData) {
    try {
        const response = await fetch(`${URL}/auth/registration`, {
            method: 'POST',
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json'
            }
        });
        const json = await response.json();
        if (response.status === 200) {
            return true
        }
        Alert.alert('Error', `${json}`);
        return false;
    }
    catch (e : Error | any) {
        Alert.alert('Error', `${e.message}`);
        return false
    }
}

export async function login(data: IData): Promise<boolean | IUserData> {
    try {
        const response = await fetch(`${URL}/auth/login`, {
            method: 'POST',
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json'
            }
        });
        const json = await response.json();
        if (response.status === 200) {
            storeTokenInfo({
                accessToken: json.accessToken,
                id: json.id,
            });
            return {
                name: json.user.name,
                surname: json.user.surname,
                email: json.user.email,
                password: json.user.password,
                avatar: json.user.avatar,
                id: json.id
            };
        }
        Alert.alert('Error', 'Such user doesn`t exist. Check your email and password or create new account')
        return false
    } catch (e : Error | any) {
        Alert.alert('Error', `${e.message}`);
        return false
    }
}

export async function getAllUserPosts() {
    try {
        const tokenInfo = getTokenInfo && await getTokenInfo();
        if (tokenInfo && typeof tokenInfo !== 'boolean') {
            const response = await fetch(`${URL}/post/getAllUserPosts`, {
                method: "GET",
                headers: {
                    'Authorization': `Bearer ${tokenInfo.accessToken}`
                }
            });
            const json = await response.json();
            if (response.status !== 200) {
                Alert.alert('Error', `${json}`);
                return [];
            }
            return json;
        }
    } catch (e : Error | any) {
        Alert.alert('Error', `${e.message}`);
        return false
    }
}

export async function addPost(data: IPost) {
    try {
        const tokenInfo = getTokenInfo && await getTokenInfo();
        if (tokenInfo && typeof tokenInfo !== 'boolean') {
            const response = await fetch(`${URL}/post/addPost`, {
                method: 'POST',
                body: JSON.stringify(data),
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': `Bearer ${tokenInfo.accessToken}`
                }
            });
            const json = await response.json();
            if (response.status !== 200) {
                return Alert.alert('Error', `${json}`);
            }
            return json;
        }
    } catch (e : Error | any) {
        Alert.alert('Error', `${e.message}`);
        return false
    }
}

export async function likePostById(id: any) {
    try {
        const tokenInfo = getTokenInfo && await getTokenInfo();
        if (tokenInfo && typeof tokenInfo !== 'boolean') {
            const response = await fetch(`${URL}/post/likePost/${id}`, {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': `Bearer ${tokenInfo.accessToken}`
                }
            });
            const json = await response.json();
            if (json == 'ok') {
                return true
            }
            return false
        }
    } catch (e : Error | any) {
        Alert.alert('Error', `${e.message}`);
        return false
    }
}

export async function deletePostById(id: any) {
    try {
        const tokenInfo = getTokenInfo && await getTokenInfo();
        if (tokenInfo && typeof tokenInfo !== 'boolean') {
            const response = await fetch(`${URL}/post/deletePost/${id}`, {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': `Bearer ${tokenInfo.accessToken}`
                }
            });
            const json = await response.json();
            if (response.status !== 200) {
                return Alert.alert('Error', `${json}`);
            }
            return json
        }
    } catch (e : Error | any) {
        Alert.alert('Error', `${e.message}`);
        return false
    }
}


export async function getUsers() {
    try {
        const tokenInfo = getTokenInfo && await getTokenInfo();
        if (tokenInfo && typeof tokenInfo !== 'boolean') {
            const response = await fetch(`${URL}/friends/allUsers`, {
                method: 'GET',
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': `Bearer ${tokenInfo.accessToken}`
                }
            });
            const json = await response.json();
            if (response.status !== 200) {
                return Alert.alert('Error', `${json}`);
            }
            return json;
        }
    } catch (e : Error | any) {
        Alert.alert('Error', `${e.message}`);
        return false
    }
}

export async function getUserFriends() {
    try {
        const tokenInfo = getTokenInfo && await getTokenInfo();
        if (tokenInfo && typeof tokenInfo !== 'boolean') {
            const response = await fetch(`${URL}/friends`, {
                method: 'GET',
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': `Bearer ${tokenInfo.accessToken}`
                }
            });
            const json = await response.json();
            if (response.status !== 200) {
                return Alert.alert('Error', `${json}`);
            }
            return json;
        }
    } catch (e : Error | any) {
        Alert.alert('Error', `${e.message}`);
        return false
    }
}


export async function sendInvitation(id: any) {
    try {
        const tokenInfo = getTokenInfo && await getTokenInfo();
        if (tokenInfo && typeof tokenInfo !== 'boolean') {
            const response = await fetch(`${URL}/friends/addFriend/${id}`, {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': `Bearer ${tokenInfo.accessToken}`
                }
            });
            const json = await response.json();
            if (response.status !== 200) {
                return Alert.alert('Error', `${json}`);
            }
            return json;
        }
    } catch (e : Error | any) {
        Alert.alert('Error', `${e.message}`);
        return false
    }
}

export async function getAllInvitations() {
    try {
        const tokenInfo = getTokenInfo && await getTokenInfo();
        if (tokenInfo && typeof tokenInfo !== 'boolean') {
            const response = await fetch(`${URL}/friends/invitations`, {
                method: 'GET',
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': `Bearer ${tokenInfo.accessToken}`
                }
            });
            const json = await response.json();
            if (response.status !== 200) {
                return Alert.alert('Error', `${json}`);
            }
            return json;
        }
    } catch (e : Error | any) {
        Alert.alert('Error', `${e.message}`);
        return false
    }
}

export async function acceptFriendInvitation(id: any) {
    try {
        const tokenInfo = getTokenInfo && await getTokenInfo();
        if (tokenInfo && typeof tokenInfo !== 'boolean') {
            const response = await fetch(`${URL}/friends/acceptInvitation/${id}`, {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': `Bearer ${tokenInfo.accessToken}`
                }
            });
            const json = await response.json();
            if (response.status !== 200) {
                return Alert.alert('Error', `${json}`);
            }
            return json;
        }
    } catch (e : Error | any) {
        Alert.alert('Error', `${e.message}`);
        return false
    }
}

export async function rejectInvit(id: any) {
    try {
        const tokenInfo = getTokenInfo && await getTokenInfo();
        if (tokenInfo && typeof tokenInfo !== 'boolean') {
            const response = await fetch(`${URL}/friends/rejectInvitation/${id}`, {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': `Bearer ${tokenInfo.accessToken}`
                }
            });
            const json = await response.json();
            if (response.status !== 200) {
                return Alert.alert('Error', `${json}`);
            }
            return json
        }
    } catch (e : Error | any) {
        Alert.alert('Error', `${e.message}`);
        return false
    }
}

export async function deleteFriend(id: string) {
    try {
        const tokenInfo = getTokenInfo && await getTokenInfo();
        if (tokenInfo && typeof tokenInfo !== 'boolean') {
            const response = await fetch(`${URL}/friends/deleteUser/${id}`, {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': `Bearer ${tokenInfo.accessToken}`
                }
            });
            const json = await response.json();
            if (response.status !== 200) {
                return Alert.alert('Error', `${json}`);
            }
            return json
        }
    } catch (e : Error | any) {
        Alert.alert('Error', `${e.message}`);
        return false
    }
}

export async function getFriendsNews() {
    try {
        const tokenInfo = getTokenInfo && await getTokenInfo();
        if (tokenInfo && typeof tokenInfo !== 'boolean') {
            const response = await fetch(`${URL}/post/news`, {
                method: "GET",
                headers: {
                    'Authorization': `Bearer ${tokenInfo.accessToken}`
                }
            });
            const json = await response.json();
            if (response.status !== 200) {
                return Alert.alert('Error', `${json}`);
            }
            return json;
        }
    } catch (e : Error | any) {
        Alert.alert('Error', `${e.message}`);
        return false
    }
}

export async function changeUserAvatar(avatar: string) {
    try {

        const tokenInfo = getTokenInfo && await getTokenInfo();
        if (tokenInfo && typeof tokenInfo !== 'boolean') {
            const response = await fetch(`${URL}/user/changeAvatar`, {
                method: 'POST',
                body: JSON.stringify({ avatar }),
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': `Bearer ${tokenInfo.accessToken}`
                }
            });
            const json = await response.json();
            if (response.status !== 200) {
                return Alert.alert('Error', `${json}`);
            }
            return true
        }
    } catch (e : Error | any) {
        Alert.alert('Error', `${e.message}`);
        return false
    }
}

export async function getUserChats() {
    try {

        const tokenInfo = getTokenInfo && await getTokenInfo();
        if (tokenInfo && typeof tokenInfo !== 'boolean') {
            const response = await fetch(`${URL}/chats/getAllChatsByUser`, {
                method: 'GET',
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': `Bearer ${tokenInfo.accessToken}`
                }
            });
            const json = await response.json();
            if (response.status !== 200) {
                return Alert.alert('Error', `${json}`);
            }
            return json
        }
    } catch (e : Error | any) {
        Alert.alert('Error', `${e.message}`);
        return false
    }
}

export async function findChatWithUser(id: string) {
    try {
        const tokenInfo = getTokenInfo && await getTokenInfo();
        if (tokenInfo && typeof tokenInfo !== 'boolean') {
            const response = await fetch(`${URL}/chats/findChat/${id}`, {
                method: 'GET',
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': `Bearer ${tokenInfo.accessToken}`
                }
            });
            const json = await response.json();
            if (response.status !== 200) {
                return Alert.alert('Error', `${json}`);
            }
            return json
        }
    } catch (e : Error | any) {
        Alert.alert('Error', `${e.message}`);
        return false
    }
}

export async function addComment(date: IComment) {
    try {
        const tokenInfo = getTokenInfo && await getTokenInfo();
        if (tokenInfo && typeof tokenInfo !== 'boolean') {
            const response = await fetch(`${URL}/comments/addComment`, {
                method: 'POST',
                body: JSON.stringify(date),
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': `Bearer ${tokenInfo.accessToken}`
                }
            });
            const json = await response.json();
            if (response.status !== 200) {
                return Alert.alert('Error', `${json}`);
            }
            return json
        }
    } catch (e : Error | any) {
        Alert.alert('Error', `${e.message}`);
        return false
    }
}

export async function deleteComment({ commentId, postId }: any) {
    try {
        const tokenInfo = getTokenInfo && await getTokenInfo();
        if (tokenInfo && typeof tokenInfo !== 'boolean') {
            const response = await fetch(`${URL}/comments/deleteComment/${commentId}`, {
                method: 'POST',
                body: JSON.stringify({ postId }),
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': `Bearer ${tokenInfo.accessToken}`
                }
            });
            const json = await response.json();
            if (response.status !== 200) {
                return Alert.alert('Error', `${json}`);
            }
            return json

        }
    } catch (e : Error | any) {
        Alert.alert('Error', `${e.message}`);
        return false
    }
}

export async function getCommentsOfPost(id: any) {
    try {
        const tokenInfo = getTokenInfo && await getTokenInfo();
        if (tokenInfo && typeof tokenInfo !== 'boolean') {
            const response = await fetch(`${URL}/comments/getCoommentsOfPost/${id}`, {
                method: 'GET',
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': `Bearer ${tokenInfo.accessToken}`
                }
            });
            const json = await response.json();
            if (response.status !== 200) {
                return Alert.alert('Error', `${json}`);
            }
            return json
        }
    } catch (e : Error | any) {
        Alert.alert('Error', `${e.message}`);
        return false
    }
}

export async function getAllMessagesFromChat(id: any) {
    try {
        const tokenInfo = getTokenInfo && await getTokenInfo();
        if (tokenInfo && typeof tokenInfo !== 'boolean') {
            const response = await fetch(`${URL}/chats/getAllMessagesFromChat/${id}`, {
                method: 'GET',
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': `Bearer ${tokenInfo.accessToken}`
                }
            });
            const json = await response.json();
            if (response.status !== 200) {
                return Alert.alert('Error', `${json}`);
            }
            return json
        }
    } catch (e : Error | any) {
        Alert.alert('Error', `${e.message}`);
        return false
    }
}