import React from 'react';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { Ionicons, Feather } from '@expo/vector-icons';
import { Profile } from '../screens/Profile';
import { News } from '../screens/News';
import { Chats } from '../screens/Chats';

const Tab = createBottomTabNavigator();

export function ProfileTabs() {
    return (
        <Tab.Navigator>
            <Tab.Screen
                name="Profile"
                component={Profile}
                options={{ headerShown: false, tabBarIcon: () => (<Feather name="user" size={24} color="black" />) }}
            />
            <Tab.Screen
                name="News"
                component={News}
                options={{ headerShown: false, tabBarIcon: () => (<Ionicons name="md-newspaper-outline" size={24} color="black" />) }}
            />
            <Tab.Screen
                name="Chats"
                initialParams={{ param: true }}
                component={Chats}
                options={{ headerShown: false, tabBarIcon: () => (<Ionicons name="chatbubbles-outline" size={24} color="black" />) }}
            />
        </Tab.Navigator>
    )
}

