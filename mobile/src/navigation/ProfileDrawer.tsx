import React from 'react';
import { createDrawerNavigator } from '@react-navigation/drawer';
import { Posts } from '../screens/Posts';
import { FriendsTabs } from './FriendsTabs';
import { ProfileTabs } from './ProfileTabs';

const Drawer = createDrawerNavigator();

export function ProfileDrawer() {

    return (
        <Drawer.Navigator>
            <Drawer.Screen name="Main page" component={ProfileTabs} options={{ headerShown: false }} />
            <Drawer.Screen name="My posts" component={Posts} options={{ headerShown: false }} />
            <Drawer.Screen name="Friends" component={FriendsTabs} options={{ headerShown: false }} />
        </Drawer.Navigator>
    )
}
