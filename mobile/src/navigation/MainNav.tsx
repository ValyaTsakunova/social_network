import React, { useContext } from 'react';
import { NavigationContainer, DrawerActions } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import { Entypo, AntDesign } from '@expo/vector-icons';
import { TouchableOpacity, Alert } from 'react-native';
import { Login } from '../screens/Login';
import { Registration } from '../screens/Registration';
import { ProfileDrawer } from './ProfileDrawer';
import Context from '../context/context';
import { CurrentChat } from '../screens/CurrentChat';

const Stack = createNativeStackNavigator();

export function MainNav() {
  const context = useContext(Context);
  const { userInfo } = context;

  const logout = (navigation: any) => {
    Alert.alert(
      "Log Out",
      "Are You sure to exit?",
      [
        {
          text: "OK", onPress: async () => {
            navigation.navigate('Login', { param: true });
          }
        },
        {
          text: "Cancel",
          style: "cancel"
        },
      ],
      { cancelable: false }
    );
  }

  return (
    <NavigationContainer>
      <Stack.Navigator initialRouteName="Login">
        <Stack.Screen name="Login" initialParams={{ param: true }} component={Login} />
        <Stack.Screen name="Registration" component={Registration} />
        <Stack.Screen
          name="profile"
          component={ProfileDrawer}
          options={({ navigation }) => ({
            title: `${userInfo.name} ${userInfo.surname}`,

            headerLeft: () => (
              <TouchableOpacity onPress={() => navigation.dispatch(DrawerActions.toggleDrawer())}>
                <Entypo
                  name="menu"
                  size={30}
                  color='black'
                  style={{
                    marginRight: 5
                  }} />
              </TouchableOpacity>
            ),
            headerRight: () => (
              <TouchableOpacity onPress={() => logout(navigation)}>
                <AntDesign name="logout" size={24} color="black" style={{
                  marginRight: 10
                }} />

              </TouchableOpacity>
            )
          })} />
        <Stack.Screen name="Current chat" component={CurrentChat} />
      </Stack.Navigator>
    </NavigationContainer>
  );
}
