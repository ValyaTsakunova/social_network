import React, { useContext } from 'react';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { FontAwesome5, SimpleLineIcons } from '@expo/vector-icons';
import Context from '../context/context';
import { FindFriends } from '../screens/FindFriends';
import { MyFriends } from '../screens/MyFriends';
import { FriendsInvitations } from '../screens/FriendsInvitations';

const Tab = createBottomTabNavigator();

export function FriendsTabs() {
    const context = useContext(Context);
    const { invitations } = context;
    
    return (
        <Tab.Navigator>
            <Tab.Screen
                name="Find friends"
                component={FindFriends}
                initialParams={{ param: true }}
                options={{ headerShown: false, tabBarIcon: () => (<FontAwesome5 name="user-plus" size={22} color="black" />) }}
            />
            <Tab.Screen
                name="My friends"
                component={MyFriends}
                initialParams={{ param: true }}
                options={{ headerShown: false, tabBarIcon: () => (<FontAwesome5 name="user-friends" size={24} color="black" />) }}
            />
            <Tab.Screen
                name="Invitations"
                component={FriendsInvitations}
                options={{ headerShown: false, tabBarBadge: invitations?.length == 0 ? undefined : invitations?.length, tabBarIcon: () => (<SimpleLineIcons name="note" size={24} color="black" />) }}
            />
        </Tab.Navigator>
    )
}

