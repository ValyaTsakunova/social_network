import React, { useState } from 'react';
import { View, Modal, Text, TextInput, TouchableOpacity, ScrollView, Alert, StyleSheet, Image } from 'react-native';
import { Entypo } from '@expo/vector-icons';
import { Video } from 'expo-av';
import dateFormat from 'dateformat';
import { addPost } from '../services/http.service';
import { PhotoFromGallery } from './PhotoFromGallery';
import { PhotoWithCamera } from './PhotoWithCamera'
import { VideoMaker } from './VideoMaker';

export function ModalNewPost({ setUserPosts, setModalVisible }: any) {
    const [title, setTitle] = useState('');
    const [text, setText] = useState('');
    const [picture, setPicture] = useState('');
    const [video, setVideo] = useState('')

    const savePost = async () => {
        if (title && text) {
            const result = await addPost({ title, text, picture, video, date: dateFormat() });
            setUserPosts(result)
            setModalVisible(false);
            return Alert.alert(`New post added`)
        }
        return Alert.alert('Error', 'For creating a new post you need add post title and text')
    }

    const handleClose = () => {
        setModalVisible(false);
        setTitle('');
        setText('')
    }

    const takePicFromGallery = (uri: string) => {
        setPicture(uri)
    }

    const takeVideo = (uri: string) => {
        setVideo(uri)
    }

    return (
        <Modal
            transparent={true}
            visible={true}
            onRequestClose={() => {
                Alert.alert('Please, save your new post first');
            }}>
            <View style={styles.modalView}>
                <TouchableOpacity style={styles.closeButton} onPress={handleClose}>
                    <Entypo name="cross" size={20} color="black" />
                </TouchableOpacity>
                <Text style={styles.modalText}>New post</Text>
                <ScrollView style={styles.info}>
                    <TextInput placeholder='type title here...' value={title} onChangeText={setTitle} style={styles.input} />
                    <TextInput multiline={true} placeholder='type text here ...' value={text} onChangeText={setText} style={styles.input} />
                    {picture ?
                        <Image source={{ uri: picture }} style={styles.picFromGallery} /> : null}
                    {video ?
                        <Video source={{ uri: video }} rate={1.0} volume={1.0} resizeMode="cover" shouldPlay style={{ width: '100%', height: 400 }} /> : null}
                </ScrollView>
                <View style={styles.addButtons}>
                    <PhotoFromGallery takePicFromGallery={takePicFromGallery} />
                    <PhotoWithCamera takePicFromGallery={takePicFromGallery} />
                    <VideoMaker takeVideo={takeVideo} />
                </View>
                <TouchableOpacity onPress={savePost}>
                    <Text style={styles.saveButton}>Save</Text>
                </TouchableOpacity>
            </View>
        </Modal>
    )
}

const styles = StyleSheet.create({
    input: {
        borderBottomWidth: 1,
        borderBottomColor: 'black',
        width: '100%',
        marginBottom: 10
    },
    info: {
        width: 320,
    },
    modalView: {
        flex: 1,
        alignItems: 'center',
        marginTop: 65,
        margin: 20,
        backgroundColor: 'rgba(247, 239, 246, 1)',
        borderRadius: 20,
        shadowColor: '#000',
        shadowOffset: {
            width: 6,
            height: 6,
        },
        shadowOpacity: 0.7,
        shadowRadius: 3.84,
        elevation: 20,
    },
    modalText: {
        fontSize: 20,
        color: 'black',
        fontFamily: 'Noto Sans KR',
    },
    saveButton: {
        margin: 10,
        color: "white",
        fontSize: 16,
        paddingLeft: 10,
        paddingRight: 10,
        borderRadius: 5,
        backgroundColor: "#9932CC"
    },
    closeButton: {
        position: 'absolute',
        right: 10,
        top: 5
    },
    addButtons: {
        flexDirection: 'row'
    },
    picFromGallery: {
        width: '100%',
        height: 300,
        marginBottom: 10
    }
})
