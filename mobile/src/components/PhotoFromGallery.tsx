import React, { useEffect } from 'react';
import { View, Platform, TouchableOpacity, StyleSheet } from 'react-native';
import { FontAwesome } from '@expo/vector-icons'; 
import * as ImagePicker from 'expo-image-picker';
import * as MediaLibrary from 'expo-media-library';

export function PhotoFromGallery(props: any) {
   
    useEffect(() => {
      (async () => {
        if (Platform.OS !== 'web') {
          const { status } = await ImagePicker.requestMediaLibraryPermissionsAsync();
          if (status !== 'granted') {
            return false
          }
          return true
        }
      })();
    }, []);
  
    const pickImage = async () => {
      let result = await ImagePicker.launchImageLibraryAsync({
        mediaTypes: ImagePicker.MediaTypeOptions.All,
        allowsEditing: true,
        aspect: [4, 3],
        quality: 1,
      });
  
      if (!result.cancelled) {
        const asset = await MediaLibrary.createAssetAsync(result.uri);
        props.takePicFromGallery(asset.uri);
      }
    };
  
    return (
      <View>
        <TouchableOpacity style={styles.block} onPress={pickImage}>
              <FontAwesome name="photo" size={24} color="black" />
          </TouchableOpacity>
      </View>
    );
  }

  const styles = StyleSheet.create({
    block: {
      flex: 1,
      alignItems: 'center',
      justifyContent: 'center',
      margin: 15
    }
  })
    