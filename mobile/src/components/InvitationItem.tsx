import React, { useContext } from 'react';
import { View, StyleSheet, TouchableOpacity, Text, Image } from 'react-native';
import { AntDesign, MaterialCommunityIcons } from '@expo/vector-icons';
import Context, { IUser } from '../context/context';
import { acceptFriendInvitation, rejectInvit } from '../services/http.service'

interface props {
    user: IUser,
    setInvit: any
}

export function InvitationItem({ user, setInvit }: props) {
    const context = useContext(Context);
    const { getInvitations } = context;

    const acceptInvitation = async () => {
        const res = await acceptFriendInvitation(user._id);
        setInvit(res);
        getInvitations && getInvitations(res)
    }
    const rejectInvitation = async () => {
        const res = await rejectInvit(user._id);
        setInvit(res);
        getInvitations && getInvitations(res);
    }

    return (
        <View style={styles.invitCard}>
            <Text style={styles.text}>Friend request from {user.name} {user.surname}</Text>
            <Image source={{ uri: user.avatar }} style={styles.img} />
            <View style={styles.buttons}>
                <TouchableOpacity onPress={acceptInvitation}>
                    <AntDesign name="adduser" size={26} color="black" />
                </TouchableOpacity>
                <TouchableOpacity onPress={rejectInvitation}>
                    <MaterialCommunityIcons name="account-cancel-outline" size={32} color="black" />
                </TouchableOpacity>
            </View>

        </View>
    )
}

const styles = StyleSheet.create({
    invitCard: {
        backgroundColor: 'rgba(247, 239, 246, 0.6)',
        margin: 5,
        width: 350,
        alignItems: 'center',
        borderRadius: 50
    },
    buttons: {
        width: '50%',
        flexDirection: 'row',
        justifyContent: 'space-around'
    },
    img: {
        width: 50,
        height: 50,
        borderRadius: 40,
        marginBottom: 10
    },
    text: {
        fontSize: 18,
        color: 'black',
        fontFamily: 'Noto Sans KR',
        marginRight: 10
    },
})

