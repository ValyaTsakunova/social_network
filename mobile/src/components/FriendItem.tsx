import React from 'react';
import { View, StyleSheet, TouchableOpacity, Text, Image, Alert } from 'react-native';
import { Ionicons, AntDesign } from '@expo/vector-icons';
import { useNavigation } from '@react-navigation/native';
import { IUser } from '../context/context';
import { deleteFriend, findChatWithUser } from '../services/http.service';

interface props {
    user: IUser,
    setFriends: any
}

export function FriendItem({ user, setFriends }: props) {
    const navigation: any = useNavigation();

    const chatToUser = async () => {
        const res = await findChatWithUser(user._id);
        navigation.navigate('Current chat', { chatId: res, user: { name: user.name, surname: user.surname, avatar: user.avatar } })
    }

    const deleteUser = () => {
        Alert.alert(
            "Delete user",
            `Do you want to delete ${user.name} ${user.surname} from your friends?`,
            [
                {
                    text: "OK", onPress: async () => {
                        const res = await deleteFriend(user._id);
                        setFriends(res)
                    }
                },
                {
                    text: "Cancel",
                    style: "cancel"
                },
            ],
            { cancelable: false }
        );
    }

    return (
        <View style={styles.userCard}>
            <Image source={{
                uri: user.avatar
            }} style={styles.img} />
            <Text style={styles.text}>{user.name} {user.surname}</Text>
            <View style={styles.buttons}>
                <TouchableOpacity onPress={chatToUser} >
                    <Ionicons name="ios-chatbox-outline" size={24} color="black" />
                </TouchableOpacity>
                <TouchableOpacity onPress={deleteUser} style={styles.delete}>
                    <AntDesign name="deleteuser" size={24} color="black" />
                </TouchableOpacity>
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    userCard: {
        flexDirection: 'row',
        backgroundColor: 'rgba(247, 239, 246, 0.6)',
        margin: 5,
        width: 350,
        height: 50,
        alignItems: 'center',
        justifyContent: 'space-between',
        borderRadius: 50,
        paddingLeft: 5,
        paddingRight: 5
    },
    text: {
        fontSize: 20,
        color: 'black',
        fontFamily: 'Noto Sans KR',
        marginRight: 10
    },
    img: {
        width: 40,
        height: 40,
        borderRadius: 40
    },
    buttons: {
        flexDirection: 'row',
    },
    delete: {
        marginLeft: 10
    }
})

