import React, { useEffect } from 'react';
import { View, Platform, TouchableOpacity } from 'react-native';
import { Feather } from '@expo/vector-icons';
import * as ImagePicker from 'expo-image-picker';
import * as MediaLibrary from 'expo-media-library';

export function VideoMaker(props: any) {
  useEffect(() => {
    (async () => {
      if (Platform.OS !== 'web') {
        const { status } = await ImagePicker.requestMediaLibraryPermissionsAsync();
        if (status !== 'granted') {
          return false
        }
        return true
      }
    })();
  }, []);

  const recordVidoe = async () => {
    let result = await ImagePicker.launchCameraAsync({
      mediaTypes: ImagePicker.MediaTypeOptions.Videos,
      quality: 1,
      allowsEditing: false,
      aspect: [9, 19]
    });

    if (!result.cancelled) {
      const asset = await MediaLibrary.createAssetAsync(result.uri);
      props.takeVideo(asset.uri);
    }
  };

  return (
    <View>
      <TouchableOpacity style={{ margin: 15 }} onPress={recordVidoe}>
        <Feather name="video" size={24} color="black" />
      </TouchableOpacity>
    </View>
  );
}