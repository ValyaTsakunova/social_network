import React, { useContext, useState, useEffect } from 'react';
import { View, StyleSheet, TouchableOpacity, Text, Image } from 'react-native';
import { AntDesign } from '@expo/vector-icons';
import Context, { IUser } from '../context/context';
import { sendInvitation } from '../services/http.service'

interface props {
    user: IUser
}

export function UserItem({ user }: props) {
    const context = useContext(Context);
    const { userInfo } = context;
    const [alreadyFriend, setAlreadyFriend] = useState(false);

    useEffect(() => {
        const res = user.friends?.filter((friend: IUser) => userInfo.id == friend._id);
        if (res?.length == 1 || userInfo.id == user._id) {
            return setAlreadyFriend(true)
        }
        return setAlreadyFriend(false)
    }, [])


    const addFriend = async () => {
        await sendInvitation(user._id);
        setAlreadyFriend(true)
    }

    return (
        <View style={styles.userCard}>
            <Image source={{
                uri: user.avatar
            }} style={styles.img} />
            <Text style={styles.text}>{user.name} {user.surname}</Text>
            <View>
                {!alreadyFriend ? <TouchableOpacity onPress={addFriend}>
                    <AntDesign name="adduser" size={24} color="black" />
                </TouchableOpacity> : null}
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    userCard: {
        flexDirection: 'row',
        backgroundColor: 'rgba(247, 239, 246, 0.6)',
        margin: 5,
        width: 300,
        height: 50,
        alignItems: 'center',
        justifyContent: 'space-between',
        borderRadius: 50,
        paddingLeft: 5,
        paddingRight: 5
    },
    text: {
        fontSize: 20,
        color: 'black',
        fontFamily: 'Noto Sans KR',
        marginRight: 10
    },
    img: {
        width: 40,
        height: 40,
        borderRadius: 40,
    }
})

