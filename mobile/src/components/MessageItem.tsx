import React from 'react';
import { View, StyleSheet, Text, Image, TouchableHighlight } from 'react-native';
import { IMessage } from '../context/context';


interface props {
    message: IMessage
}

export function MessageItem({ message }: props) {
    return (
        <View style={styles.messageCard}>
            <Image style={styles.img} source={{ uri: message.author.avatar }} />
            <Text style={styles.text}>{message.text}</Text>
            <TouchableHighlight style={styles.time}>
                <Text>{message.date}</Text>
            </TouchableHighlight>
        </View>
    )
}

const styles = StyleSheet.create({
    messageCard: {
        flexDirection: 'row',
        alignItems: 'center',
        backgroundColor: 'rgba(247, 239, 246, 0.6)',
        borderRadius: 20,
        margin: 5
    },
    text: {
        width: '70%',
        fontSize: 15,
        color: 'black',
        fontFamily: 'Noto Sans KR',
        marginRight: 10
    },
    img: {
        width: 40,
        height: 40,
        borderRadius: 40,
        margin: 10
    },
    time: {
        position: 'absolute',
        right: 10,
        bottom: 5
    }
})

