import React from "react";
import { StyleSheet, View } from "react-native";
import LottieView from "lottie-react-native";

export function Loader() {
  return (
    <View style={styles.animation}>
      <LottieView
        source={require("../../assets/68800-loader-icon.json")}
        style={styles.loader}
        autoPlay
      />
    </View>

  );
}

const styles = StyleSheet.create({
  animation: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    width: '100%',
    height: '100%',
  },
  loader: {
    width: 250,
    height: 250,
  }
});