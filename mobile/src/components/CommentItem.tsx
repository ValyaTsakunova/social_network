import React, { useContext } from 'react';
import { View, StyleSheet, Text, Image, Alert, TouchableHighlight, TouchableOpacity } from 'react-native';
import { Foundation } from '@expo/vector-icons';
import Context, { IComment } from '../context/context';
import { deleteComment } from '../services/http.service';

interface props {
    comment: IComment,
    setComments: any,
    setComm: any
}

export function CommentItem({ comment, setComments, setComm }: props) {
    const context = useContext(Context);
    const { userInfo } = context;

    const deleteComm = () => {
        Alert.alert(
            "Delete user",
            `Do you want to delete your comment?`,
            [
                {
                    text: "OK", onPress: async () => {
                        const res = await deleteComment({ postId: comment.postId, commentId: comment._id });
                        setComments(res);
                        setComm(res.length);
                    }
                },
                {
                    text: "Cancel",
                    style: "cancel"
                },
            ],
            { cancelable: false }
        );
    }

    return (
        <View style={styles.commCard}>
            <View style={styles.user}>
                {userInfo.id == comment.authorId ? <TouchableOpacity style={styles.deleteButton} onPress={deleteComm} >
                    <Foundation name="page-delete" size={20} color="grey" />
                </TouchableOpacity> : null}
                <Image source={{ uri: comment.authorAvatar }} style={styles.img} />
                <Text style={styles.userName}>{comment.authorName} {comment.authorSurname}</Text>
            </View>
            <Text style={styles.text}>{comment.text}</Text>
            <TouchableHighlight style={styles.date}>
                <Text>{comment.date.slice(0, 21)}</Text>
            </TouchableHighlight>
        </View>
    )
}

const styles = StyleSheet.create({
    commCard: {
        width: '100%',
        alignItems: 'center',
        borderRadius: 10,
        borderWidth: 1,
        borderColor: 'black',
        padding: 5,
        marginBottom: 5,
        backgroundColor: 'rgba(247, 239, 246, 0.6)',
    },
    text: {
        width: '95%',
        fontSize: 15,
        color: 'black',
        fontFamily: 'Noto Sans KR',
        textAlign: 'left'
    },
    userName: {
        color: 'black',
        fontFamily: 'Noto Sans KR',
    },
    img: {
        width: 40,
        height: 40,
        borderRadius: 40,
        marginRight: 10,
    },
    user: {
        width: '90%',
        flexDirection: 'row',
        alignItems: 'flex-end',
        justifyContent: 'flex-start',
    },
    date: {
        width: '90%',
        flexDirection: 'row',
        justifyContent: 'flex-end',
        margin: 5,
        opacity: 0.6
    },
    deleteButton: {
        position: 'absolute',
        right: 0,
        top: 5,
    },
})