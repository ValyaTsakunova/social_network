import React, { useEffect } from 'react';
import { View, TouchableOpacity, Platform } from 'react-native';
import * as ImagePicker from 'expo-image-picker';
import { MaterialIcons } from '@expo/vector-icons';
import * as MediaLibrary from 'expo-media-library';

export function PhotoWithCamera(props: any) {
  useEffect(() => {
    (async () => {
      if (Platform.OS !== 'web') {
        const { status } = await ImagePicker.requestMediaLibraryPermissionsAsync();
        if (status !== 'granted') {
          return false
        }
        return true
      }
    })();
  }, []);

  const pickImage = async () => {
    let result = await ImagePicker.launchCameraAsync({
      mediaTypes: ImagePicker.MediaTypeOptions.Images,
      quality: 0.7,
      allowsEditing: false,
      aspect: [16, 9],
      videoMaxDuration: 17
    });

    if (!result.cancelled) {
      const asset = await MediaLibrary.createAssetAsync(result.uri);
      props.takePicFromGallery(asset.uri);
    }
  };

  return (
    <View>
      <TouchableOpacity style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }} onPress={pickImage} >
        <MaterialIcons name="add-a-photo" size={24} color="black" />
      </TouchableOpacity>
    </View>
  );
}
