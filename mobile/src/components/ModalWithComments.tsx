import React, { useState, useContext, useEffect, useRef } from 'react';
import { View, Modal, Text, TextInput, TouchableOpacity, ScrollView, Alert, StyleSheet, Image, Keyboard } from 'react-native';
import { Entypo, MaterialCommunityIcons } from '@expo/vector-icons';
import Context, { IComment, IPost } from '../context/context';
import { addComment, getCommentsOfPost } from '../services/http.service';
import { CommentItem } from './CommentItem';
import dateFormat from 'dateformat';

interface props {
    post: IPost,
    setModalVisible: any,
    setComm: any
}

export function ModalWithComments({ post, setModalVisible, setComm }: props) {
    const context = useContext(Context);
    const { userInfo } = context;
    const [commentText, setCommentText] = useState('');
    const [comments, setComments] = useState<Array<IComment>>([]);

    const scrollViewRef = useRef<any>()

    useEffect(() => {
        (async () => {
            const res = await getCommentsOfPost(post._id);
            setComments(res);
        })()
    }, []);

    const handleClose = () => {
        setModalVisible(false);
    }

    const addCommentToPost = async () => {
        if (!commentText) {
            return Alert.alert(`Warning`, `Comment next should't be empty`)
        }
        const comment = {
            text: commentText,
            authorName: userInfo.name,
            authorSurname: userInfo.surname,
            authorAvatar: userInfo.avatar,
            authorId: userInfo.id,
            postId: post._id,
            date: dateFormat()
        }
        const res = await addComment(comment);
        setComm(res);

        const resp = await getCommentsOfPost(post._id);
        setComments(resp);
        setCommentText('');
    }

    useEffect(() => {
        scrollViewRef.current?.scrollToEnd({ animated: true })
      }, [comments.length])

    return (
        <Modal
            transparent={true}
            visible={true}
            onRequestClose={() => {
                Alert.alert('Please, save your new post first');
            }}>
            <View style={styles.modalView}>
                <TouchableOpacity style={styles.closeButton} onPress={handleClose}>
                    <Entypo name="cross" size={20} color="black" />
                </TouchableOpacity>
                <Text style={styles.modalText}>Comments</Text>
                <ScrollView style={styles.comments} ref={scrollViewRef}>
                    {comments.length !== 0 ? comments.map((comm: IComment) => { return (<CommentItem comment={comm} key={comm._id} setComments={setComments} setComm={setComm} />) }) : null}
                </ScrollView>
                <View style={styles.sendComment}>
                    <TextInput
                        style={styles.input}
                        multiline={true}
                        placeholder='Type your comment here ...'
                        value={commentText}
                        onChangeText={setCommentText}
                    />
                    <TouchableOpacity onPress={addCommentToPost}>
                        <MaterialCommunityIcons name="send" size={25} color="black" />
                    </TouchableOpacity>
                </View>
            </View>
        </Modal>
    )
}

const styles = StyleSheet.create({
    input: {
        borderBottomWidth: 1,
        borderBottomColor: 'black',
        width: '80%',
        marginBottom: 10,
        marginRight: 15
    },
    modalView: {
        flex: 1,
        alignItems: 'center',
        marginTop: 65,
        margin: 20,
        backgroundColor: '#b7d4c0',
        borderRadius: 20,
        shadowColor: '#000',
        shadowOffset: {
            width: 6,
            height: 6,
        },
        shadowOpacity: 0.7,
        shadowRadius: 3.84,
        elevation: 20,
    },
    modalText: {
        fontSize: 20,
        color: 'black',
        fontFamily: 'Noto Sans KR',
    },

    closeButton: {
        position: 'absolute',
        right: 10,
        top: 5
    },
    sendComment: {
        flexDirection: 'row',
        margin: 10,
        alignItems: 'center',
    },
    comments: {
        width: '90%'
    }

})
