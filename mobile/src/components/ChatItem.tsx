import React, { useContext, useState, useEffect } from 'react';
import { View, StyleSheet, TouchableOpacity, Text, Image } from 'react-native';
import { Ionicons } from '@expo/vector-icons';
import Context, { IChat, IUser } from '../context/context';
import { useNavigation } from '@react-navigation/native';

interface props {
    chat: IChat,
}

export function ChatItem({ chat }: props) {
    const navigation: any = useNavigation();
    const context = useContext(Context);
    const { userInfo } = context;
    const [chatWithUser, setChatWithUser] = useState<any>({}); //

    useEffect(() => {
        if (chat.users[0]._id == userInfo.id) {
            return setChatWithUser(chat.users[1])
        }
        return setChatWithUser(chat.users[0])
    }, []);
    const openChat = () => {
        navigation.navigate('Current chat', { chatId: chat._id, user: { name: chatWithUser.name, surname: chatWithUser.surname, avatar: chatWithUser.avatar } })
    }

    return (
        <View style={styles.chatCard}>
            <Image source={{
                uri: chatWithUser.avatar
            }} style={styles.img} />
            <Text style={styles.text}>{chatWithUser.name} {chatWithUser.surname}</Text>
            <TouchableOpacity onPress={openChat}>
                <Ionicons name="mail-open-outline" size={24} color="black" />
            </TouchableOpacity>
        </View>
    )
}

const styles = StyleSheet.create({
    chatCard: {
        flexDirection: 'row',
        backgroundColor: 'rgba(247, 239, 246, 0.6)',
        width: 350,
        height: 50,
        alignItems: 'center',
        justifyContent: 'space-between',
        borderRadius: 50,
        marginBottom: 5,
        paddingLeft: 10,
        paddingRight: 10
    },
    text: {
        fontSize: 20,
        color: 'black',
        fontFamily: 'Noto Sans KR',
        marginRight: 10
    },
    img: {
        width: 40,
        height: 40,
        borderRadius: 40,
    }
})