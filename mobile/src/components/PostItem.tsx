import React, { useState } from 'react';
import { View, StyleSheet, TouchableOpacity, Text, Image, Alert } from 'react-native';
import { Video } from 'expo-av';
import { FontAwesome, Foundation } from '@expo/vector-icons';
import { IPost } from '../context/context';
import { deletePostById, getAllUserPosts, getFriendsNews, likePostById } from '../services/http.service'
import { ModalWithComments } from './ModalWithComments';

interface props {
    post: IPost,
    setUserPosts?: any,
    setFriendsNews?: any
}

export function PostItem({ post, setUserPosts, setFriendsNews }: props) {
    const [modalVisible, setModalVisible] = useState(false);
    const [comm, setComm] = useState(post.comments?.length);

    const likePost = async () => {
        const res = await likePostById(post._id);
        if (res) {
            setUserPosts && setUserPosts(await getAllUserPosts());
            setFriendsNews && setFriendsNews(await getFriendsNews());
        }
    }

    const openComments = () => {
        setModalVisible(true)
    }

    const deletePost = async () => {
        Alert.alert(
            "Delete user",
            `Do you want to delete this post?`,
            [
                {
                    text: "OK", onPress: async () => {
                        setUserPosts(await deletePostById(post._id));
                    }
                },
                {
                    text: "Cancel",
                    style: "cancel"
                },
            ],
            { cancelable: false }
        );
    }

    return (
        <View style={styles.block}>
            <View style={styles.userInfoBlock}>
                <Image style={styles.img} source={{ uri: post.user?.avatar }} />
                <View>
                    <Text style={{ fontSize: 15 }}>{post.user?.name} {post.user?.surname}</Text>
                    <Text style={{ fontSize: 12, color: 'grey' }}>{post.date.slice(0, 21)}</Text>
                </View>
            </View>
            {setUserPosts ?
                <TouchableOpacity style={styles.deleteButton} onPress={deletePost} >
                    <Foundation name="page-delete" size={20} color="grey" />
                </TouchableOpacity>
                : null}
            <Text style={styles.title}>{post.title}</Text>
            <Text style={styles.text}>{post.text}</Text>
            {post.picture ? <Image source={{ uri: post.picture }} style={styles.picFromGallery} /> : null}
            {post.video ? <Video source={{ uri: post.video }} rate={1.0} style={styles.video} volume={1.0} resizeMode="cover" shouldPlay /> : null}
            <View style={styles.likesAndComments}>
                <View style={styles.comments}>
                    <TouchableOpacity style={{ marginRight: 7 }} onPress={openComments} >
                        <FontAwesome name="comments-o" size={24} color="black" />
                    </TouchableOpacity>
                    <Text>{comm}</Text>
                </View>
                <View style={styles.likes}>
                    <TouchableOpacity style={{ marginRight: 7 }} onPress={likePost} >
                        <FontAwesome name="heart" size={24} color="red" />
                    </TouchableOpacity>
                    <Text>{post.likes?.length}</Text>
                </View>
                {modalVisible ? <ModalWithComments post={post} setModalVisible={setModalVisible} setComm={setComm} /> : null}
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    block: {
        flexDirection: 'column',
        backgroundColor: 'rgba(247, 239, 246, 1)',
        borderRadius: 5,
        padding: 10,
        width: '95%',
        marginHorizontal: 10,
        alignItems: 'center',
        marginBottom: 20,
    },
    userInfoBlock: {
        flexDirection: 'row',
        width: '100%',
        justifyContent: 'flex-start',
        alignItems: 'center'
    },
    title: {
        fontSize: 20,
        color: 'black',
        fontFamily: 'Noto Sans KR',
        marginTop: 5
    },
    text: {
        fontSize: 16,
        color: 'black',
        fontFamily: 'Noto Sans KR',
        marginTop: -20
    },
    picFromGallery: {
        width: '100%',
        height: 300,
        marginBottom: 10
    },
    likesAndComments: {
        flexDirection: 'row',
        width: '100%',
        justifyContent: 'flex-end'
    },
    video: {
        width: '100%',
        height: 300,
        marginBottom: 10,
    },
    comments: {
        flexDirection: 'row',
    },
    likes: {
        flexDirection: 'row',
        marginLeft: 10
    },
    deleteButton: {
        position: 'absolute',
        right: 10,
        top: 5,
    },
    img: {
        width: 40,
        height: 40,
        borderRadius: 40,
        marginRight: 10
    },
})

