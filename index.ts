import { Request, Response } from 'express';
import { authRouter, friendsRouter, postRouter, chatRouter, userRouter, commentRouter } from './routers/index';
import { authenticateJWT } from './controllers/authController';
import { socketStart } from './controllers/socket'

const bodyParser = require('body-parser');
const express = require('express');
const mongoose = require('mongoose');
const app = express();

app.use(bodyParser.urlencoded({ extended: true }));
app.use(express.json());

app.use("/auth", authRouter);
app.use("/post", authenticateJWT, postRouter);
app.use("/friends", authenticateJWT, friendsRouter);
app.use("/chats", authenticateJWT, chatRouter);
app.use("/user", authenticateJWT, userRouter);
app.use("/comments", authenticateJWT, commentRouter);

app.use("/", function (request: Request, response: Response) {
  response.send(`Main page`);
});

mongoose.connect('mongodb://localhost:27017/social', {
  useNewUrlParser: true,
  useCreateIndex: true,
  useUnifiedTopology: true,
  useFindAndModify: false
}, () => {
  console.log('connected to database')
})

const socket = require('socket.io');
const server = app.listen(3000, () => {
  console.log('server is listening on port 3000')
})
export const io = socket(server);
socketStart(io);

export {app};
 