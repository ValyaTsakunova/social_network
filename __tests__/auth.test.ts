import { app } from "../index";
const request = require("supertest");
export let token: any;

describe("auth", () => {
    // it("POST registration", async () => {
    //   const resp = await request(app)
    //     .post("/auth/registration")
    //     .send({
    //       name: "new user",
    //       surname: 'new user',
    //       email: 'new user',
    //       password: 'new user'
    //     });
    //   expect(resp.statusCode).toBe(200);
    //   expect(resp.text).toBe(`Registration successfully.`);
    // });

    it("POST login", async () => {
      const resp = await request(app)
        .post("/auth/login")
        .send({
          email: 'new user',
          password: 'new user'
        });
      const json = JSON.parse(resp.text)
      token = json.accessToken;
      expect(resp.statusCode).toBe(200);
      expect(json.user.name).toBe('new user');
      expect(json.user.surname).toBe('new user');
    })
  });

  const mongoose = require('mongoose');
  afterAll(done => {
    mongoose.connection.close();
    done();
  });

