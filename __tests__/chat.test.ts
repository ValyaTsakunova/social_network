import { app } from "../index";
import { token } from './auth.test'
const request = require("supertest");

describe("chats", () => {
    it("GET all chats by user", async () => {
        const resp = await request(app)
            .get("/chats/getAllChatsByUser")
            .set('authorization', `Bearer ${token}`)
            .set('Content-Type',  'application/json')
        const result = JSON.parse(resp.text);
        expect(resp.statusCode).toBe(200);
        expect(Array.isArray(result)).toBe(true);
    });
});

const mongoose = require('mongoose');
afterAll(done => {
    mongoose.connection.close();
    done();
});