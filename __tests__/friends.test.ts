import { app } from "../index";
import { token } from './auth.test'
const request = require("supertest");

describe("friends", () => {
    it("GET get friends of user", async () => {
        const resp = await request(app)
            .get("/friends")
            .set('authorization', `Bearer ${token}`)
            .set('Content-Type',  'application/json')
        const friends = JSON.parse(resp.text);
        expect(resp.statusCode).toBe(200);
        expect(Array.isArray(friends)).toBe(true);
    });

    it("GET all users", async () => {
        const resp = await request(app)
            .get("/friends/allUsers")
            .set('authorization', `Bearer ${token}`)
            .set('Content-Type',  'application/json')
        const users = JSON.parse(resp.text);
        expect(resp.statusCode).toBe(200);
        expect(Array.isArray(users)).toBe(true);
    });

    it("POST add friend", async () => {
        const resp = await request(app)
            .post("/friends/addFriend/6135c87a7e9cd02db81509fc")
            .set('authorization', `Bearer ${token}`)
            .set('Content-Type',  'application/json')
        const result = JSON.parse(resp.text);
        expect(resp.statusCode).toBe(200);
        expect(result).toBe('added friend');
    });

    it("GET friend invitations", async () => {
        const resp = await request(app)
            .get("/friends/invitations")
            .set('authorization', `Bearer ${token}`)
            .set('Content-Type',  'application/json')
        const result = JSON.parse(resp.text);
        expect(resp.statusCode).toBe(200);
        expect(Array.isArray(result)).toBe(true);
    });

    it("POST accept invitation", async () => {
        const resp = await request(app)
            .post("/friends/acceptInvitation/6135c87a7e9cd02db81509fc")
            .set('authorization', `Bearer ${token}`)
            .set('Content-Type',  'application/json')
        const result = JSON.parse(resp.text);
        expect(resp.statusCode).toBe(200);
        expect(Array.isArray(result)).toBe(true);
    });
});

const mongoose = require('mongoose');
afterAll(done => {
    mongoose.connection.close();
    done();
});